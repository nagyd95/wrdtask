package com.example.pollbackend.controller;

import com.example.pollbackend.Repository.AnswerRepository;
import com.example.pollbackend.Repository.QuestionRepository;
import com.example.pollbackend.entity.Answer;
import com.example.pollbackend.entity.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/questionnaire")
@CrossOrigin
public class QuestionController {
    @Autowired
    private QuestionRepository questionRepository;

    @GetMapping("")
    private List<Question> getAll(){
        return questionRepository.getAll();
    }
}
