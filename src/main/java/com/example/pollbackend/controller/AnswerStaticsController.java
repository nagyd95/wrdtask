package com.example.pollbackend.controller;

import com.example.pollbackend.entity.Question;
import com.example.pollbackend.entity.ResponseTransfer;
import com.example.pollbackend.service.StaticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/answer-statistics")
@CrossOrigin
public class AnswerStaticsController {

    @Autowired
    private StaticsService service;
    @RequestMapping("/{questionId}")
    public ResponseTransfer getStatic(@PathVariable(value = "questionId") Long questionId){
        return service.getStatic(questionId);
    }
}
