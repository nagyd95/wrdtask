package com.example.pollbackend.controller;

import com.example.pollbackend.Repository.SavedAnswerRepository;
import com.example.pollbackend.entity.Question;
import com.example.pollbackend.entity.SavedAnswer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/answers")
@CrossOrigin
public class AnswerController {
    @Autowired
    private SavedAnswerRepository answerRepository;

    @PostMapping("")
    private String saveAnswers(@ModelAttribute SavedAnswer savedAnswer){
        System.out.println(savedAnswer);
        answerRepository.save(savedAnswer);

        return "ok";
    }
    @GetMapping("/getAll")
    private List<SavedAnswer> getAll(){
        return answerRepository.getAll();
    }
}
