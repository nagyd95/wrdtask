package com.example.pollbackend.service;

import com.example.pollbackend.Repository.AnswerRepository;
import com.example.pollbackend.Repository.QuestionRepository;
import com.example.pollbackend.Repository.SavedAnswerRepository;
import com.example.pollbackend.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class StaticsService {
    @Autowired
    private SavedAnswerRepository savedAnswerRepository;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private AnswerRepository answerRepository;
    public ResponseTransfer getStatic(Long questionId) {
        Map<Long,Integer> statistic= new HashMap<>();
        Optional<Question> question= questionRepository.findById(questionId);
        ResponseTransfer responseTransfer= new ResponseTransfer();
        if(question.isPresent()) {
            List<SavedAnswer> savedAnswerList=savedAnswerRepository.getById(questionId);
            List<Integer> frequency= new ArrayList<>();
            for (SavedAnswer answer : savedAnswerList) {
                frequency.addAll(answer.getAnswers());
            }
            for(Answer q : question.get().getAnswers()){
                statistic.put(q.getId(),Collections.frequency(frequency,q.getId()));
            }

            responseTransfer.setQuestion(question.get().getQuestion());
            responseTransfer.setType(question.get().getType().getType());
            ArrayList<ResponseTransferInsade> responseTransferInsades= new ArrayList<>();
            for(Long localMap: statistic.keySet()){
                ResponseTransferInsade responses = new ResponseTransferInsade();
                responses.setText(answerRepository.getNameById(localMap).getAnswer());
                responses.setCount(statistic.get(localMap));
                responseTransferInsades.add(responses);
            }
            responseTransfer.setAnswers(responseTransferInsades);
        }
        return responseTransfer;
    }
}
