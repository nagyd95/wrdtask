package com.example.pollbackend.entity;

import com.example.pollbackend.Repository.TypeRepository;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "type")
@Getter
@Setter
@NoArgsConstructor
public class Type {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @Column(name = "type")
    private String type;
    public Type(String type)  {
        this.type = type;
    }
}
