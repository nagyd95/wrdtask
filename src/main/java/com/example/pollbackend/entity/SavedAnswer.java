package com.example.pollbackend.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
@Table(name = "savedAnswer")
@NoArgsConstructor
@Getter
@Setter
public class SavedAnswer {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column(nullable = false)
    private int question_id;

    @Column
    private ArrayList<Integer> answers;

}
