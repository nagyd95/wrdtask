package com.example.pollbackend.entity;

import java.util.ArrayList;


public class ResponseTransfer {
    private String question;
    private String type;
    ArrayList<ResponseTransferInsade> answers;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<ResponseTransferInsade> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<ResponseTransferInsade> answers) {
        this.answers = answers;
    }
}
