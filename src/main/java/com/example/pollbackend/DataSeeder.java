package com.example.pollbackend;

import com.example.pollbackend.Repository.AnswerRepository;
import com.example.pollbackend.Repository.QuestionRepository;
import com.example.pollbackend.Repository.TypeRepository;
import com.example.pollbackend.entity.Answer;
import com.example.pollbackend.entity.Question;
import com.example.pollbackend.entity.Type;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class DataSeeder implements ApplicationListener<ContextRefreshedEvent> {
    private final AnswerRepository answerRepository;

    private final QuestionRepository questionRepository;

    private final TypeRepository typeRepository;

    public DataSeeder(AnswerRepository answerRepository, QuestionRepository questionRepository, TypeRepository typeRepository) {
        this.answerRepository = answerRepository;
        this.questionRepository = questionRepository;
        this.typeRepository = typeRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        Answer angular = new Answer("Angular");
        Answer vue = new Answer("Vue");
        Answer react = new Answer("React");
        Answer webstorm = new Answer("Webstorm");
        Answer intelliJ = new Answer("IntelliJ IDEA");
        Answer VSCode = new Answer("VSCode");
        Answer Sublime = new Answer("Sublime");
        Type one= new Type("SELECT_ONE");
        Type two= new Type("SELECT_MORE");
        Type free= new Type("FREE_TEXT");


        typeRepository.save(one);
        typeRepository.save(two);
        typeRepository.save(free);

        Set<Answer> answers = new HashSet<>();
        answers.add(angular);
        answers.add(vue);
        answers.add(react);

        Set<Answer> answers2 = new HashSet<>();
        answers2.add(webstorm);
        answers2.add(intelliJ);
        answers2.add(VSCode);
        answers2.add(Sublime);

        Question question1=new Question();
        question1.setQuestion("Which javascript framework do you like the most?");
        question1.setType(one);
        question1.setAnswers(answers);
        questionRepository.save(question1);

        Question question2=new Question();
        question2.setQuestion("Which IDEs do you like to work with?");
        question2.setType(two);
        question2.setAnswers(answers2);
        questionRepository.save(question2);

        Question question3=new Question();
        question3.setQuestion("What's your proudest moment so far?");
        question3.setType(free);
        questionRepository.save(question3);

    }
}
