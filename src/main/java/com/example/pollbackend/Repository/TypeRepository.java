package com.example.pollbackend.Repository;

import com.example.pollbackend.entity.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TypeRepository extends JpaRepository<Type, Integer> {

    @Query("Select c from Type c")
    List<Type> getAll();
}
