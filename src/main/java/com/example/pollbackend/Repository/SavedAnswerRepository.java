package com.example.pollbackend.Repository;


import com.example.pollbackend.entity.Question;
import com.example.pollbackend.entity.SavedAnswer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SavedAnswerRepository extends CrudRepository<SavedAnswer, Long> {
    @Query("Select ans from SavedAnswer ans")
    List<SavedAnswer> getAll();

    @Query("Select ans from SavedAnswer ans where ans.id= :id")
    List<SavedAnswer> getById(@Param("id") Long id);
}
