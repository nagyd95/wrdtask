package com.example.pollbackend.Repository;

import com.example.pollbackend.entity.Answer;
import com.example.pollbackend.entity.Question;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AnswerRepository extends CrudRepository<Answer, Long> {
    @Query(value = "Select q from Answer q where q.id= :id ")
    Answer getNameById(@Param("id") Long id);
}
