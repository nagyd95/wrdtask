package com.example.pollbackend.Repository;

import com.example.pollbackend.entity.Question;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface QuestionRepository extends CrudRepository<Question, Long> {
    @Query("Select c from Question c")
    List<Question> getAll();
}
